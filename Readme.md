## NLDR
NonLocal Douglas-Rachford (NLDR) splitting algorithm for Compressed Sensing (CS) image recovery

## Usage
run "demo_main.m"

## Contributors
Shuangjiang Li(sli22@vols.utk.edu), Hairong Qi (hqi@utk.edu), EECS, University of Tennessee, Knoxville

## Reference:
1. Shuangjiang Li and Hairong Qi, "A Douglas-Rachford Splitting Approach to Compressed Sensing Image Recovery using Low-rank Regularization", submitted to TIP.