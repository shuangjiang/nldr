function [rec_im rec_im_tmp PSNR]   =  nldr_rec( par, A, At  )
time0      =   clock;
y          =   par.y;
rec_im0    =   dct_rec( y, par, A, At );

PSNR_IST = csnr( rec_im0, par.ori_im, 0, 0 )

rec_im     =   rec_im0;
cnt        =   0;

rec_im_tmp = cell(1,25);

for  k    =   1 : par.K %  DA algorithm with 28 iter. here
    
    blk_arr    =   Block_matching( rec_im, par);
    f          =   rec_im;
    U_arr      =   zeros(par.win^4, size(blk_arr,2), 'single');
    if (k<=par.K0)  flag=0;  else flag=1;  end
    
    for it  =  1 : par.iters % 15 here
        cnt      =   cnt  +  1;
        if (mod(cnt, 20) == 0)
            if isfield(par,'ori_im')
                rec_im_tmp{cnt/20} = f;
                PSNR(cnt/20)     =   csnr( f, par.ori_im, 0, 0 );
                fprintf( 'NLDR CS Recovery, Iter %d : PSNR = %f \n', cnt, PSNR(cnt/20) );
                %imwrite(f./255, 'Results\DR_NL_Temp.tif')
            end
            par.nSig=par.nSig*0.95;
        end
        
        for i = 1 : par.nn
            fb     =   A( f(:) );
            f      =   f + par.lamada.*reshape(At( y-fb ), par.h, par.w);
%             if ( k < 2)
%                 psnr_nn_iter = csnr( f, par.ori_im, 0, 0 );
%                 fprintf( 'Iter %d :, par.nn %d: PSNR = %f \n', k, i, psnr_nn_iter );
%             end
        end
        [f U_arr]    =   nldr_denoise( f, par, blk_arr, U_arr, it, flag );
    end
    
    rec_im   =  f;
end
disp(sprintf('Total elapsed time = %f min\n', (etime(clock,time0)/60) ));
return;



function   [dim U_arr]  =  nldr_denoise(nim, par, blk_arr, U_arr, it, flag)
b            =   par.win;
[h  w ch]    =   size(nim);
N            =   h-b+1;
M            =   w-b+1;
r            =   [1:N];
c            =   [1:M];

X            =   Im2Patch( nim, par );

Ys        =   zeros( size(X) );
W         =   zeros( size(X) );
L         =   size(blk_arr,2);

for  i  =  1 : L
    B          =   X(:, blk_arr(:, i));
    mB         =   repmat(mean( B, 2 ), 1, size(B, 2)); %imshow(reshape(B(:,2),6,6)./255);
    B          =   B-mB;
    
    % http://perception.csl.illinois.edu/matrix-rank/sample_code.html
    if it==1 || mod(it, par.T)==0
        [Ys(:, blk_arr(:,i)), W(:, blk_arr(:,i)), U_arr(:,i)] = DR_splitting( double(B), par.c1, par.nSig^2, mB, flag, par.c0 );
    else
        [Ys(:, blk_arr(:,i)), W(:, blk_arr(:,i))]   =   DR_splitting_fast( double(B), par.c1, par.nSig^2, mB, U_arr(:,i), flag, par.c0 );
    end
    
end

dim     =  zeros(h,w);
wei     =  zeros(h,w);
k       =   0;
for i  = 1:b
    for j  = 1:b
        k    =  k+1;
        dim(r-1+i,c-1+j)  =  dim(r-1+i,c-1+j) + reshape( Ys(k,:)', [N M]);
        wei(r-1+i,c-1+j)  =  wei(r-1+i,c-1+j) + reshape( W(k,:)', [N M]);
    end
end
dim   =  dim./(wei+eps);
return;


% Douglas Rachford splitting algorithm
function  [X W U]   =   DR_splitting( Y, c1, nsig2, m, flag, c0 )
niter = 2;
gamma = 1;
lambda = .01;
for i=1:niter
    Y= ProxG(Y,gamma*lambda,c1,nsig2,flag, c0 );
end

[Y wei U] = ProxG_L(Y,gamma*lambda,c1,nsig2,flag, c0 );
X = wei*(Y+m);
W  =   wei*ones( size(X) );
return;

function [X] = ProxG(Y,gamma,c1,nsig2,flag, c0)
[a b c] = svd(full(Y),'econ');
Sigma0            =   diag(b);
if flag==1
    S                 =   max( Sigma0.^2/size(Y, 2) - nsig2, 0 );
    thr               =   c1*nsig2./ ( sqrt(S) + eps );
    S                 =   soft(Sigma0, thr);
else
    S                 =   soft(Sigma0, c0*nsig2);
end
r                 =   sum( S>0 );
aa                 =   a(:,1:r);
cc                 =   c(:,1:r);
X                =   aa*diag(S(1:r))*cc';
return

function [X wei U] = ProxG_L(Y,gamma,c1, nsig2,flag, c0)

[a b c] = svd(full(Y),'econ');
Sigma0            =   diag(b);
if flag==1
    S                 =   max( Sigma0.^2/size(Y, 2) - nsig2, 0 );
    thr               =   c1*nsig2./ ( sqrt(S) + eps );
    S                 =   soft(Sigma0, thr);
else
    S                 =   soft(Sigma0, c0*nsig2);
end
r                 =   sum( S>0 );
aa                 =   a(:,1:r);
cc                 =   c(:,1:r);
X                =   aa*diag(S(1:r))*cc';
if r==size(Y,1)
    wei           =   1/size(Y,1);
else
    wei           =   (size(Y,1)-r)/size(Y,1);
end
U = a(:);
return

function  [X W]   =   DR_splitting_fast( Y, c1, nsig2, m, U0, flag, c0 )
niter = 2;
gamma = 1;
lambda = .01;
for i=1:niter
    Y=  ProxG_fast( Y, c1, nsig2, m, U0, flag, c0 );
end
[Y wei] = ProxG_fast_L(Y,gamma*lambda,nsig2, m, U0, flag, c0 );
X = wei*(Y+m);
W  =   wei*ones( size(X) );
return;

function  [X]   =   ProxG_fast( Y, c1, nsig2, m, U0, flag, c0 )
n                 =   sqrt(length(U0));
U0                =   reshape(U0, n, n);
A                 =   U0'*Y;
Sigma0            =   sqrt( sum(A.^2, 2) );
V0                =   (diag(1./Sigma0)*A)';

if flag==1
    S                 =   max( Sigma0.^2/size(Y, 2) - nsig2, 0 );
    thr               =   c1*nsig2./ ( sqrt(S) + eps );
    S                 =   soft(Sigma0, thr);
else
    S                 =   soft(Sigma0, c0*nsig2);
end
r                 =   sum( S>0 );
P                 =   find(S);
X                 =   U0(:,P)*diag(S(P))*V0(:,P)';
return;

function  [X wei]   =   ProxG_fast_L( Y, c1, nsig2, m, U0, flag, c0 )
n                 =   sqrt(length(U0));
U0                =   reshape(U0, n, n);
A                 =   U0'*Y;
Sigma0            =   sqrt( sum(A.^2, 2) );
V0                =   (diag(1./Sigma0)*A)';

if flag==1
    S                 =   max( Sigma0.^2/size(Y, 2) - nsig2, 0 );
    thr               =   c1*nsig2./ ( sqrt(S) + eps );
    S                 =   soft(Sigma0, thr);
else
    S                 =   soft(Sigma0, c0*nsig2);
end
r                 =   sum( S>0 );
P                 =   find(S);
X                 =   U0(:,P)*diag(S(P))*V0(:,P)';
if r==size(Y,1)
    wei           =   1/size(Y,1);
else
    wei           =   (size(Y,1)-r)/size(Y,1);
end
return

% CS recovery using DCT measurement matrix
function  im  = dct_rec( y, par, A, At )
ori_im      =   par.ori_im;
[h w]       =   size(ori_im);
im          =   At( y );
im          =   reshape(im,[h w]);

lamada      =   par.lamada;
b           =   par.win*par.win;
D           =   dctmtx(b);
% DTD = D'*D; imagesc(DTD)
% DTD should be an identity matrix
% D is the orthogonal matrix i.e., D'D = DD' = I;

for k   =  1:1
    f      =   im;
    for  iter = 1 : 300
        if (mod(iter, 50) == 0)
            if isfield(par,'ori_im')
                PSNR     =   csnr( f, ori_im, 0, 0 );
                fprintf( 'DCT CS Recovery, Iter %d : PSNR = %f\n', iter, PSNR );
                %imwrite(f./255, 'Results\DCT_Temp.tif')
            end
        end
        
        for ii = 1 : par.nn
            fb        =   A( f(:) );
            f         =   f + lamada.*reshape(At( y-fb ), h, w);
        end
        f          =   DCT_thresholding( f, par, D );
    end
    im     =  f;
end
return;


